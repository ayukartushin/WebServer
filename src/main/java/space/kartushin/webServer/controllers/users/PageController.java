package space.kartushin.webServer.controllers.users;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import space.kartushin.webServer.models.PageModel;

@Controller
public class PageController {
    @GetMapping("/")
    public String index(Model model) {
        PageModel indexPage = PageModel.builder()
                .title("Главная страница")
                .name("index")
                .build();
        model.addAttribute("page", indexPage);
        return "indexTemplate";
    }

    @GetMapping("/catalog")
    public String catalog(Model model) {
        PageModel indexPage = PageModel.builder()
                .title("Каталог сайта")
                .name("catalog")
                .build();
        model.addAttribute("page", indexPage);
        return "indexTemplate";
    }

    @GetMapping("/about")
    public String about(Model model) {
        PageModel indexPage = PageModel.builder()
                .title("О нас")
                .name("about")
                .build();
        model.addAttribute("page", indexPage);
        return "indexTemplate";
    }
}
