package space.kartushin.webServer.controllers.api.v1.products;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import space.kartushin.webServer.controllers.api.CrudApi;
import space.kartushin.webServer.models.Product;
import space.kartushin.webServer.repo.ProductRepository;

@RestController
@RequestMapping("/api/v1/product")
public class ProductsApi extends CrudApi<Product, ProductRepository> {
}
