package space.kartushin.webServer.controllers.api.v1.category;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import space.kartushin.webServer.controllers.api.CrudApi;
import space.kartushin.webServer.models.Category;
import space.kartushin.webServer.repo.CategoryRepository;

@RestController
@RequestMapping("/api/v1/category")
public class CategoryApi extends CrudApi<Category, CategoryRepository> {

}
