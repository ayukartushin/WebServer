package space.kartushin.webServer.controllers.api;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.*;
import space.kartushin.webServer.models.IResponse;
import space.kartushin.webServer.models.Response;

import static space.kartushin.webServer.models.enums.Status.*;

@RestController
public abstract class CrudApi<T extends IResponse, Repo extends CrudRepository> {

    @Autowired
    protected Repo repository;

    @GetMapping("/")
    public Iterable<T> get() {
        Iterable<T> response = repository.findAll();
        return response;
    }

    @GetMapping("/{id}")
    public IResponse get(@PathVariable long id) {
        val obj = repository.findById(id);
        if (obj.isPresent()) {
            return (T) obj.get();
        } else {
            return Response.builder()
                    .status(FAIL)
                    .message("No such element")
                    .build();
        }
    }

    @PostMapping("/")
    public IResponse create(@RequestBody T body) {
        if (!isAdmin())
            return Response.builder()
                    .status(FAIL)
                    .message("No permissions")
                    .build();

        T obj = (T) repository.save(body);

        return Response.builder()
                .status(OK)
                .message(obj.toString())
                .build();
    }

    @PutMapping("/{id}")
    public IResponse update(@PathVariable long id, @RequestBody T body) {
        if (!isAdmin())
            return Response.builder()
                    .status(FAIL)
                    .message("No permissions")
                    .build();

        if (repository.existsById(id)) {
            repository.save(body);
            return Response.builder().status(OK).build();
        } else {
            return Response.builder()
                    .status(FAIL)
                    .message("No such element")
                    .build();
        }
    }

    @DeleteMapping("/{id}")
    public IResponse delete(@PathVariable long id) {
        if (!isAdmin())
            return Response.builder()
                    .status(FAIL)
                    .message("No permissions")
                    .build();

        if (repository.existsById(id)) {
            repository.deleteById(id);
            return Response.builder().status(OK).build();
        } else {
            return Response.builder()
                    .status(FAIL)
                    .message("No such element")
                    .build();
        }
    }

    protected boolean isAdmin(){
        //TODO добавить проверку на админские права
        return true;
    }
}
