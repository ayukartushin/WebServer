package space.kartushin.webServer.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import space.kartushin.webServer.models.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
}
