package space.kartushin.webServer.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import space.kartushin.webServer.models.Category;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {
}
