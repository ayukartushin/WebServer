package space.kartushin.webServer.models;

import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import space.kartushin.webServer.models.enums.Status;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonSerialize
public class Response implements IResponse {
    Status status;
    @Builder.Default
    String message = "";
}
