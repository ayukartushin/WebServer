package space.kartushin.webServer.models;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.persistence.*;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@ToString
@EqualsAndHashCode
@JsonSerialize
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Category implements IResponse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;
    String name;
    String imgUrl;
    String description;
}
