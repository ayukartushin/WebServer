package space.kartushin.webServer.models;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PageModel {
    String title;
    String name;
}
