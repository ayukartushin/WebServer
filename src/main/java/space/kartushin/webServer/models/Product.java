package space.kartushin.webServer.models;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.persistence.*;
import java.util.ArrayList;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@ToString
@EqualsAndHashCode
@JsonSerialize
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Product implements IResponse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;
    long categoryId;
    String name;
    String[] imgUrl;
    String description;
}
